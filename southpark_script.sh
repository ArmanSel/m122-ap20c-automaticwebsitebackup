#!/bin/bash

# SouthPark Website Backup Automatisierungsskript
# Dieses Skript automatisiert den Prozess des Backups der South Park-Themenwebsite.

# Konfigurationsdatei
CONFIG_FILE="southpark_automation.cfg"

# Hauptfunktion
main() {
    # Überprüfen, ob die Konfigurationsdatei existiert
    if [ -f "$CONFIG_FILE" ]; then
        # Einlesen der Konfigurationsdatei
        source "$CONFIG_FILE"

	# Code für die Hauptfunktionalität des Skripts
download_files() {

# Verzeichnis des Log Files
LOG_DIR="$HOME/m122-ap20c-automaticwebsitebackup/logfiledir"

# Überprüfen, ob das Verzeichnis für die Logdateien existiert, andernfalls erstellen
if [ ! -d "$LOG_DIR" ]; then
    mkdir -p "$LOG_DIR"
fi

# Logdatei basierend auf dem aktuellen Datum erstellen
LOG_FILE="$LOG_DIR/$(date +%Y-%m-%d).log"

    # Erstellen des Backup-Speicherorts basierend auf dem aktuellen Datum
    backup_folder="$BACKUP_LOCATION/$(date +%Y-%m-%d)"

    # Überprüfen, ob der Backup-Ordner bereits existiert, falls nicht, erstellen
    if [ ! -d "$backup_folder" ]; then
	mkdir -p "$backup_folder"
    fi

    # Wechseln in den Backup-Speicherort
    cd "$backup_folder" || exit

    # Herunterladen der Website-Dateien
    wget -r -nH --cut-dirs=1 --no-parent --reject "index.html*" "ftp://$FTP_USERNAME:$FTP_PASSWORD@$FTP_SERVER/"

    # Überprüfen, ob der Download erfolgreich war
    if [ $? -eq 0 ]; then
        echo "Website-Backup erfolgreich durchgeführt."
send_notification() {
    subject="Website-Backup abgeschlossen"
    message="Das Backup der South Park-Themenwebsite wurde erfolgreich abgeschlossen."
    echo "$(date) - Skript erfolgreich beendet." >> "$LOG_FILE"
    echo "$message" | mail -s "$subject" "$ADMIN_EMAIL"
}


    # Komprimieren des Backups basierend auf der Konfiguration
                if [ "$COMPRESSION_FORMAT" = "zip" ]; then
                    zip -r website_backup.zip *
                elif [ "$COMPRESSION_FORMAT" = "tar.gz" ]; then
                    tar -czvf website_backup.tar.gz *
                else
                    echo "Ungültiges Komprimierungsformat in der Konfigurationsdatei."
		    echo "$(date) - Skript mit Fehlern beendet." >> "$LOG_FILE"
		exit 1
                fi

else
        echo "Fehler beim Durchführen des Website-Backups."
echo "$(date) - Skript mit Fehlern beendet." >> "$LOG_FILE"
exit 1
    fi
}

# Aufruf der Funktion zum Herunterladen der Website-Dateien
download_files

    else
        echo "Fehler: Konfigurationsdatei nicht gefunden."
echo "$(date) - Skript mit Fehlern beendet." >> "$LOG_FILE"        
exit 1
    fi
}

# Aufruf der Hauptfunktion
main
# Aufruf der Funktion zum Senden der Benachrichtigung
send_notification
