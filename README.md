# Betriebsdokumentation
## Installationsanleitung für Administratoren
1. Voraussetzungen:

- Stellen Sie sicher, dass ein Ubuntu-Server oder eine entsprechende Linux-Distribution vorhanden ist.
- Überprüfen Sie, ob die erforderlichen Pakete wie wget und zip installiert sind. Falls nicht, installieren Sie sie mit den folgenden Befehlen:

```
sudo apt update
sudo apt install wget zip
```

2. Installationsschritte:

- Navigieren Sie in das gewünschte Verzeichnis, in dem das Skript gespeichert werden soll.
- Klonen Sie das GitLab-Repository mit dem Befehl:

```
git clone https://gitlab.com/ArmanSel/m122-ap20c-automaticwebsitebackup.git
```

3. Konfiguration:

- Öffnen Sie die Datei southpark_automation.cfg im Repository und passen Sie die Konfiguration an Ihre Anforderungen an.
- Stellen Sie sicher, dass die FTP-Serverinformationen und der Backupspeicherort korrekt konfiguriert sind.

4. Automatisierung:

- Führen Sie den folgenden Befehl aus, um den CronJob einzurichten und das Skript automatisch alle 10 Minuten auszuführen:

```
crontab -e
```

- Fügen Sie die folgende Zeile hinzu und speichern Sie die Datei:

```
*/10 * * * * /home/ubuntu/m122-ap20c-automaticwebsitebackup/southpark_script.sh >> /home/ubuntu/m122-ap20c-automaticwebsitebackup/logfiledir/cron.log 2>&1
```

- Beachten Sie, dass der Pfad /home/ubuntu/m122-ap20c-automaticwebsitebackup an Ihren tatsächlichen Speicherort angepasst werden muss.

## Bedienungsanleitung für Benutzer

1. Starten des Programms:

- Das Skript wird automatisch gemäss dem festgelegten CronJob-Plan ausgeführt.
- Um den Status des Backups oder Fehlermeldungen zu überprüfen, öffnen Sie die Logdatei im Verzeichnis /home/ubuntu/m122-ap20c-automaticwebsitebackup/logfiledir. Zum Beispiel:

```
cat /home/ubuntu/m122-ap20c-automaticwebsitebackup/logfiledir/2023-06-26.log
```

2. Anpassung der Konfiguration:

- Um die Konfiguration anzupassen, öffnen Sie die Datei southpark_automation.cfg im Repository und ändern Sie die entsprechenden Werte.

- Speichern Sie die Änderungen und führen Sie das Skript erneut aus, um die neuen Einstellungen anzuwenden.

3. Behandlung von Fehlern:

- Wenn das Backup fehlschlägt oder Fehler auftreten, überprüfen Sie die Logdatei auf mögliche Fehlermeldungen.

- Achten Sie darauf, dass die Konfiguration korrekt ist und die Zugriffsrechte auf die erforderlichen Verzeichnisse richtig gesetzt sind.

## Aktivitätsdiagramm

![Aktivitätsdiagramm](https://i.ibb.co/pjFyhfj/uml.png)